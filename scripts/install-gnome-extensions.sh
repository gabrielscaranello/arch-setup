#! /bin/bash

PWD=$(pwd)
EXTENSIONS=$(sed "s/[^0-9]//g" ./gnome_extensions)

echo "Installing gnome extensions..."

for extension in "${EXTENSIONS[@]}"; do
	gnome-shell-extension-installer --yes $extension
done

echo "Gnome extensions installed."
