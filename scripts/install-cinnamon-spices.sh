#! /bin/bash

APPLETS=("color-picker@fmete" "qredshift@quintao")
EXTENSIONS=("transparent-panels@germanfr")
DOWNLOAD_URL="https://cinnamon-spices.linuxmint.com/files"
CINNAMON_DIR="$HOME/.local/share/cinnamon"
CINNAMON_CONFIG_DIR="$HOME/.config/cinnamon"
APPLETS_DIR="$CINNAMON_DIR/applets"
EXTENSIONS_DIR="$CINNAMON_DIR/extensions"

_install_cinnamon_spice() {
	local package="$1"
	local type="$2"
	local destination="$3"
	local file_name="$package.zip"
	local output_file="/tmp/$file_name"
	local download_url="$DOWNLOAD_URL/$type/$file_name"

	echo "Installing $package..."
	wget -c "$download_url" -O "$output_file" >/dev/null 2>&1
	unzip "$output_file" -d "$destination" >/dev/null 2>&1
}

echo "Installing Cinnamon Spices..."

echo "Removing old files if exists..."
rm -rf "$APPLETS_DIR" "$EXTENSIONS_DIR" "$CINNAMON_CONFIG_DIR"

echo "Installing Cinnamon Spices..."
mkdir -p "$APPLETS_DIR" "$EXTENSIONS_DIR" "$CINNAMON_CONFIG_DIR"

echo "Installing Cinnamon Applets..."
for applet in "${APPLETS[@]}"; do _install_cinnamon_spice "${applet}" "applets" "$APPLETS_DIR"; done

echo "Installing Cinnamon Extensions..."
for extension in "${EXTENSIONS[@]}"; do _install_cinnamon_spice "${extension}" "extensions" "$EXTENSIONS_DIR"; done
