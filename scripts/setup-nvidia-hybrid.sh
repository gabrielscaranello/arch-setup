#! /bin/bash

PWD=$(pwd)
PACKAGES=$(cat "$PWD/nvidia-packages" | tr '\n' ' ')

echo "Configuring NVIDIA hybrid drivers..."

echo "Disabling gdm"
# sudo systemctl disable gdm

echo "Installing driver packages"
yay --noconfirm && yay -Sy --noconfirm $PACKAGES

echo "Enabling services..."
# sudo systemctl enable gdm
sudo systemctl enable nvidia-hibernate.service
sudo systemctl enable nvidia-powerd.service
sudo systemctl enable nvidia-resume.service
sudo systemctl enable switcheroo-control.service

echo "Change to nvidia driver usign envycontrol..."
sudo envycontrol --dm gdm -s nvidia

echo "NVIDIA hybrid drivers configured."
