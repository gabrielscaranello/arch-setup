#! /bin/bash

echo "Enabling services..."

sudo systemctl enable bluetooth
sudo systemctl enable cups
sudo systemctl enable docker
sudo systemctl enable gdm
sudo systemctl enable power-profiles-daemon.service

echo "Services enabled."
